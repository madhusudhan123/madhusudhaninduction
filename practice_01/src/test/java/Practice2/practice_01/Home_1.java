package Practice2.practice_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Home_1 
{
	WebDriver driver0;
	
	@FindBy(how=How.XPATH,using="//a[@class='login']")
	private WebElement sign_in_btn;//click
	Home_1(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void home_methods()
	{
		sign_in_btn.click();
	}
}
