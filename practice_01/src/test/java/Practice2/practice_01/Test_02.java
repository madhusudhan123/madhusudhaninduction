package Practice2.practice_01;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test_02 
{
	@Test
	  public void f1() 
	  {
		  System.setProperty("webdriver.gecko.driver", "./software/geckodriver.exe");
		  WebDriver driver = new FirefoxDriver();
		  driver.get("http://automationpractice.com/index.php");
		  driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
		  Home_1 h1= new Home_1(driver);
		  h1.home_methods();
		  Login_1 l1 =new Login_1(driver);
		  l1.login_methods("bigbangxyz321@gmail.com","penny123");
		  Add_to_cart_n_checkout ad1 = new Add_to_cart_n_checkout(driver);
		  ad1.atcnco();
		  WebElement ele = driver.findElement(By.className("alert alert-success"));
		  Assert.assertTrue(ele.isDisplayed());
		  System.out.println(ele.getText());
	  }
}
