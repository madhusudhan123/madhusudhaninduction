package Practice2.practice_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Signup_1 
{
	WebDriver driver1;
	
	@FindBy(how=How.ID,using="email_create")
	private WebElement cacceaddress;//sendkeys
	@FindBy(how=How.ID,using="SubmitCreate")
	private WebElement caccbutton;//click
	
	Signup_1(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void signup_1_methods(String eaddress)
	{
		cacceaddress.sendKeys(eaddress);
		caccbutton.click();
	}
}
