package Practice2.practice_01;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class Test_01 {
	public WebDriver driver ;
	@BeforeMethod
	void before()
	{
		  System.setProperty("webdriver.gecko.driver", "./software/geckodriver.exe");
		  driver = new FirefoxDriver();
		  driver.get("http://automationpractice.com/index.php");
	}
  @Test(priority=1)
  public void sign_up() 
  {
	  driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	  Home_1 h1= new Home_1(driver);
	  h1.home_methods();
	  Signup_1 s1 = new Signup_1(driver);
	  s1.signup_1_methods("bigbangxyz333@gmail.com");
	  Signup_2 s2 = new Signup_2(driver);
	  s2.signup_2_methods_pinfo("Shelly","lenord","penny123","10  ","May ", "1995  ");
	  s2.signup_2_methods_myaddrs("cap","rajnagar","pencilvania","NY","California","00000","United States"," ","1234567812","1234543212","aliasvalley");
	  WebElement ele1 = driver.findElement(By.xpath("//span[.='My account']"));
	  Assert.assertTrue(ele1.isDisplayed());
	  System.out.println(ele1.getText());
  }
  @Test(priority=2)
  public void log_in() 
  {
	  driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	  Home_1 h1= new Home_1(driver);
	  h1.home_methods();
	  Login_1 l1 =new Login_1(driver);
	  l1.login_methods("bigbangxyz333@gmail.com","penny123");
	  WebElement ele1 = driver.findElement(By.xpath("//span[.='My account']"));
	  Assert.assertTrue(ele1.isDisplayed());
	  System.out.println(ele1.getText());
  }
  @Test(priority=3)
  public void add_n_cout() 
  {
	  driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	  Home_1 h1= new Home_1(driver);
	  h1.home_methods();
	  Login_1 l1 =new Login_1(driver);
	  l1.login_methods("bigbangxyz32123@gmail.com","penny123");
	  driver.findElement(By.className("home")).click();
	  Add_to_cart_n_checkout ad1 = new Add_to_cart_n_checkout(driver);
	  ad1.atcnco();
	  WebElement ele = driver.findElement(By.className("alert alert-success"));
	  Assert.assertTrue(ele.isDisplayed());
	  System.out.println(ele.getText());
  }
}
