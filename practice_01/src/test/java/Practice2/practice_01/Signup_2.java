package Practice2.practice_01;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
public class Signup_2 
{
	WebDriver driver2;
	@FindBy(how=How.ID,using="id_gender1")
	private WebElement malerdbtn;//click
	@FindBy(how=How.ID,using="customer_firstname")
	private WebElement fntxtbox;//sendkeys
	@FindBy(how=How.ID,using="customer_lastname")
	private WebElement lntxtbox;//sendkeys
	@FindBy(how=How.ID,using="passwd")
	private WebElement pwdtxtbox;//sendkeys
	@FindBy(how=How.ID,using="days")
	private WebElement dayselector;//select
	@FindBy(how=How.ID,using="months")
	private WebElement monthselector;//select
	@FindBy(how=How.ID,using="years")
	private WebElement yearselector;//select
	@FindBy(how=How.ID,using="newsletter")
	private WebElement nlcheckbox;//click
	@FindBy(how=How.ID,using="optin")
	private WebElement offerscheckbox;//click
	//my address
	@FindBy(how=How.ID,using="company")
	private WebElement company;//sendkeys
	@FindBy(how=How.ID,using="address1")
	private WebElement add1tbox;//sendkeys
	@FindBy(how=How.ID,using="address2")
	private WebElement add2tbox;//sendkeys
	@FindBy(how=How.ID,using="city")
	private WebElement citytbox;//sendkeys
	@FindBy(how=How.ID,using="id_state")
	private WebElement stateselector;//select
	@FindBy(how=How.ID,using="postcode")
	private WebElement ziptbox;//sendkeys
	@FindBy(how=How.ID,using="id_country")
	private WebElement countryselector;//select
	@FindBy(how=How.ID,using="other")
	private WebElement addinfotafield;//sendkeys
	@FindBy(how=How.ID,using="phone")
	private WebElement hphtbox;//sendkeys
	@FindBy(how=How.ID,using="phone_mobile")
	private WebElement mphtbox;//sendkeys
	@FindBy(how=How.ID,using="alias")
	private WebElement aliasaddtbox;//sendkeys
	@FindBy(how=How.ID,using="submitAccount")
	private WebElement regbtn;//click
	
	Signup_2(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void signup_2_methods_pinfo(String fn,String ln,String pwd,String date,String month,String year)
	{

		malerdbtn.click();
		fntxtbox.sendKeys(fn);
		lntxtbox.sendKeys(ln);
		pwdtxtbox.sendKeys(pwd);
		Select s1 = new Select(dayselector);
		s1.selectByVisibleText(date);
		Select s2 = new Select(monthselector);
		s2.selectByVisibleText(month);
		Select s3 = new Select(yearselector);
		s3.selectByVisibleText(year);
		nlcheckbox.click();
		offerscheckbox.click();
	}
	public void signup_2_methods_myaddrs(String cname,String add1,String add2,String city,String state,String zipc,String country,String ainfo,String hphno,String mphno,String myaddrs)
	{
		company.sendKeys(cname);
		add1tbox.sendKeys(add1);
		add2tbox.sendKeys(add2);
		citytbox.sendKeys(city);
		Select s4 = new Select(stateselector);
		s4.selectByVisibleText(state);
		ziptbox.sendKeys(zipc);
		Select s5 = new Select(countryselector);
		s5.selectByVisibleText(country);
		addinfotafield.sendKeys(ainfo);
		hphtbox.sendKeys(hphno);
		mphtbox.sendKeys(mphno);
		aliasaddtbox.sendKeys(myaddrs);
		regbtn.click();
	}
}
