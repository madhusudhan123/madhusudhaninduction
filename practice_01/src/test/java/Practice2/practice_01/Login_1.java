package Practice2.practice_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Login_1 
{
	WebDriver driver1;
	@FindBy(how=How.ID,using="email")
	private WebElement efield;//sendkeys
	@FindBy(how=How.ID,using="passwd")
	private WebElement pwdfield;//sendkeys
	@FindBy(how=How.ID,using="SubmitLogin")
	private WebElement linbtm;//sendkeys
	Login_1(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void login_methods(String eid,String pwd)
	{
		efield.sendKeys(eid);
		pwdfield.sendKeys(pwd);
		linbtm.click();
	}
}
