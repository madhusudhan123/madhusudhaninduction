package Practice2.practice_01;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Add_to_cart_n_checkout 
{
	WebDriver driver4;
	@FindBy(how=How.CLASS_NAME,using="home")
	private WebElement home;//click
	@FindBy(how=How.CLASS_NAME,using="ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line first-item-of-tablet-line first-item-of-mobile-line")
	private WebElement pdt;//move to
	@FindBy(how=How.CLASS_NAME,using="button ajax_add_to_cart_button btn btn-default")
	private WebElement addtocartbtn;//click
	@FindBy(how=How.CLASS_NAME,using="btn btn-default button button-medium")
	private WebElement ptco1;//click
	@FindBy(how=How.CLASS_NAME,using="button btn btn-default standard-checkout button-medium")
	private WebElement ptco2;//click
	@FindBy(how=How.CLASS_NAME,using="button btn btn-default button-medium")
	private WebElement ptco3;//click
	@FindBy(how=How.ID,using="cgv")
	private WebElement termscbox;//click
	@FindBy(how=How.CLASS_NAME,using="button btn btn-default standard-checkout button-medium")
	private WebElement ptco4;//click
	@FindBy(how=How.CLASS_NAME,using="cheque")
	private WebElement paymethod;//click
	@FindBy(how=How.CLASS_NAME,using="button btn btn-default button-medium")
	private WebElement confirmbtn;//click
	
	Add_to_cart_n_checkout(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void atcnco()
	{
		home.click();
		Actions a1 = new Actions(driver4);
		a1.moveToElement(pdt).perform();
		addtocartbtn.click();
		ptco1.click();
		ptco2.click();
		ptco3.click();
		termscbox.click();
		ptco4.click();
		paymethod.click();
		confirmbtn.click();
	}
}
