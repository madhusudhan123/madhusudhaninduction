package pom;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test1 
{
	
  @Test
  public void f() 
  {
	  System.setProperty("webdriver.gecko.driver", "./software/geckodriver.exe");
	  WebDriver driver = new FirefoxDriver();
	  Login_page lp = new Login_page(driver);
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  driver.get("https://www.facebook.com");
	  lp.login_method("7676123481", "msan052902!");
	  String value = driver.getTitle();
	  System.out.println(value);
	  Assert.assertTrue(value.contentEquals("Facebook"));
  }
}
