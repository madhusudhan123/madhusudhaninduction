package pom;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class Login_page 
{
	WebDriver driver;
	@FindBy(how=How.ID,using="email")
	private WebElement untbox;
	@FindBy(how=How.ID,using="pass")
	private WebElement pwdtbox;
	@FindBy(how=How.XPATH,using="//input[@value='Log In']")
	private WebElement login;	
	Login_page(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void login_method(String un,String pwd)
	{
		untbox.sendKeys(un);
		pwdtbox.sendKeys(pwd);
		login.click();
	}
}
