import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
public class Test1 
{	
	@DataProvider(name="testdata")
	public Object[][] TestDataFeed()
	{ 
		Object [][] fd=new Object[3][3]; 
		fd[0][0]="7676123481";
		fd[0][1]="msan052902!";
		fd[0][2]="//div[.='Madhusudhan Sreenivas']";
		fd[1][0]="71";
		fd[1][1]="msan052902!";
		fd[1][2]="//a[.='Sign up for an account.']";
		fd[2][0]="7676123481";
		fd[2][1]="m12312!";
		fd[2][2]="//a[.='Forgotten password?']";
return fd;
}
@Test(dataProvider="testdata")

  public void f1(String un,String pwd,String path) 
  {	
	  System.setProperty("webdriver.gecko.driver", "./software/geckodriver.exe");
	  WebDriver driver = new FirefoxDriver();
	  Login_page lp = new Login_page(driver);
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  driver.get("https://www.facebook.com");
	  lp.login_method(un,pwd);
	  WebElement e1 = driver.findElement(By.xpath(path));
	  Assert.assertTrue(e1.isDisplayed());
	  System.out.println(e1.getText());
  }

}